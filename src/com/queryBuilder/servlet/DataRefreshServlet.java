package com.queryBuilder.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLDecoder;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;

import com.queryBuilder.builer.BuilderBasic;
import com.queryBuilder.util.QueryType;

/**
 * 初始化和刷新左侧数据列
 * 
 * @author pengfei
 * 
 */
public class DataRefreshServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1906658459314537297L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		super.doGet(req, resp);
	}

	@SuppressWarnings("resource")
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse response)
			throws ServletException, IOException {
		req.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("UTF-8");
		response.setContentType("application/json; charset=utf-8");
		PrintWriter out = null;
		String action = req.getParameter("action");
		BuilderBasic builderBasic = new BuilderBasic();
		String returnValue = "";
		JSONArray jsonArray = null;
		try {
			if (action.trim().equals(QueryType.TEMPLATE)) {
				jsonArray = builderBasic.getAllQueryTemplate();
				returnValue = jsonArray.toString();

			} else if (action.trim().equals(QueryType.ALLCLASSES)) {
				// 编辑查询模板
				String templateName = req.getParameter("templateName");
				templateName = URLDecoder.decode(templateName,"utf-8");
				returnValue = builderBasic.getAllEntityByTemplateName(templateName);
			}else if (action.equals(QueryType.ADDTEMPLATE)){
				String templateName = req.getParameter("templateName");
				String [] clsName = req.getParameter("entities").split(",");
				//新增模板
				boolean saveType = builderBasic.addTemplate(templateName,clsName,null);
				returnValue  = "{\"saveType\":"+String.valueOf(saveType)+"}";
			}else if(action.equals(QueryType.UPDATETEMPLATE)){
				String templateName = req.getParameter("templateName");
				String [] clsName = req.getParameter("entities").split(",");
				//新增模板
				boolean saveType = builderBasic.updateTemplate(templateName,clsName,null);
				returnValue  = "{\"saveType\":"+String.valueOf(saveType)+"}";
			}else if(action.equals(QueryType.EDITTEMPLATENAME)){
				//编辑
				String oldName = req.getParameter("oldName");
				String newName = req.getParameter("newName");
				boolean saveType = builderBasic.editTemplateName(oldName,newName);
				returnValue  = "{\"result\":"+String.valueOf(saveType)+"}";
			}else if(action.equals(QueryType.EDITALIANAME)){
				//编辑别名
				String oldName = req.getParameter("oldName");
				String newName = req.getParameter("newName");
				String fullName = req.getParameter("templateName");
				boolean saveType = builderBasic.editAliaName(fullName,oldName,newName);
				returnValue  = "{\"result\":"+String.valueOf(saveType)+"}";
			}else if(action.equals(QueryType.ADDALIANAME)){
				String fullName = req.getParameter("fullName");
				String aliaName = req.getParameter("aliaName");
				returnValue = builderBasic.addAliaName(fullName,aliaName);
			}else if(action.equals(QueryType.GETALIAPRO)){
				String fullName = req.getParameter("fullName");
				returnValue = builderBasic.getAliaPro(fullName);
			}else if(action.equals(QueryType.UPDATEPROS)){
				String fullName = req.getParameter("fullName");
				String pros = req.getParameter("pros");
				returnValue = builderBasic.editAliaName(fullName,pros);
			}else if(action.equals(QueryType.DELETEDATA)){
				String deleteData = req.getParameter("deleteData");
				returnValue = builderBasic.editAliaName(deleteData);
			}
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (out != null) {
				out.close();
			}
		}
		System.out.println(returnValue);
		out = response.getWriter();
		out.write(returnValue);
	}
}
