package com.queryBuilder.servlet;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class DetailIpServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7031627970936192614L;

	@SuppressWarnings("deprecation")
	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");   
		String realpath = request.getRealPath("/WEB-INF/classes")
				+ File.separator + "ip.text";
//		System.err.println(realpath);
		List<Map<String, String>> list = getAllIps(realpath);
		request.setAttribute("ips", list);
		request.setAttribute("count", getReviewCount(request));
		request.getRequestDispatcher("detailIp.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		super.doPost(req, resp);
	}

	/**
	 * 获取所有访问者的IP
	 * 
	 * @param filePath
	 * @return
	 */
	private static List<Map<String, String>> getAllIps(String filePath) {
		List<Map<String, String>> list = new ArrayList<Map<String, String>>();
		try {
			String encoding = "GBK";
			File file = new File(filePath);
			if (file.isFile() && file.exists()) { // 判断文件是否存在
				InputStreamReader read = new InputStreamReader(
						new FileInputStream(file), encoding);// 考虑到编码格式
				BufferedReader bufferedReader = new BufferedReader(read);
				String lineTxt = null;
				Map<String, String> map = null;
				while ((lineTxt = bufferedReader.readLine()) != null) {
					try{
//						System.out.println(lineTxt);
						map = new HashMap<String, String>();
						String [] tmep = lineTxt.trim().split(",");
						map.put("ip", tmep[0]);
						map.put("visitDate", tmep[1]);
						map.put("area", tmep[2]);
						list.add(map);
						
					}catch(Exception e){
						System.out.println("该行数据有错");
					}
				}
				read.close();
			} else {
				System.out.println("找不到指定的文件");
			}
		} catch (Exception e) {
			System.out.println("读取文件内容出错");
			e.printStackTrace();
		}
		return list;
	}

	/**
	 * 获取访问者数量
	 * 
	 * @param request
	 * @return
	 */
	@SuppressWarnings("deprecation")
	private String getReviewCount(HttpServletRequest request) {
		Properties pro = new Properties();
		String realpath = request.getRealPath("/WEB-INF/classes");
		String count = "";
		try {
			// 读取配置文件
			FileInputStream in = new FileInputStream(realpath
					+ "/sum.properties");
			pro.load(in);
			count = pro.getProperty("sum");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return count;
	}
}
