package com.queryBuilder.entity;

import java.util.Date;

public class VisitorInfo {

	private String ip;
	private String area;
	private String ipComPany;
	private Date visitDate;

	public Date getVisitDate() {
		return visitDate;
	}

	public void setVisitDate(Date visitDate) {
		this.visitDate = visitDate;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getIpComPany() {
		return ipComPany;
	}

	public void setIpComPany(String ipComPany) {
		this.ipComPany = ipComPany;
	}

}
